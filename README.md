# The Dancord Level XP Finder
**Purpose:**
This program is made to find how many minutes it would take to reach a specified level in the Mr Dan Discord Sings server.

**How to use:**
You need to be able to run COBOL code to use this program. I recommend you use GNU Cobol to compile it, which can be found [here](https://gnucobol.sourceforge.io/). Alternatively, you could just run it on replit [here](https://replit.com/@rileycats44/Dancord-XP-Level-Thingy?v=1)
