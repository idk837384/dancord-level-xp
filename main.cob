       IDENTIFICATION DIVISION.
       PROGRAM-ID. LEVEL-XP-FINDER.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       77  LEVEL PIC 9(6).
       77  MINUTES PIC 9(10).
       77  HOURS PIC 9(8).
       77  MATH-MEM PIC 9(36).
       PROCEDURE DIVISION.
       GET-VAR.
           DISPLAY "Input level:"
           ACCEPT LEVEL
           SUBTRACT 1 FROM LEVEL.
       MAIN-PROC.
           MULTIPLY 4 BY LEVEL GIVING MATH-MEM
           ADD MATH-MEM TO MINUTES
           SUBTRACT 1 FROM LEVEL
           IF LEVEL NOT EQUAL TO ZEROS
               GO TO MAIN-PROC
           ELSE
               ADD 1 TO MINUTES
               DISPLAY "Minutes wasted to achieve level: " MINUTES
               DIVIDE MINUTES BY 60 GIVING HOURS
               DISPLAY "Hours wasted to achieve level: " HOURS
               STOP RUN.
       END PROGRAM LEVEL-XP-FINDER.
